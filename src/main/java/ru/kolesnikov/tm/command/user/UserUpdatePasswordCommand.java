package ru.kolesnikov.tm.command.user;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.util.TerminalUtil;

public class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "user-update-password";
    }

    @Override
    public String description() {
        return "Update user password.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PASSWORD]");
        System.out.print("ENTER NEW PASSWORD: ");
        final String newPassword = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getAuthService().updatePassword(userId,newPassword);
        System.out.println("[THE PASSWORD WAS SUCCESSFULLY CHANGED]");
        serviceLocator.getAuthService().logout();
        System.out.println("[LOG IN TO YOUR ACCOUNT AGAIN]");
    }

}