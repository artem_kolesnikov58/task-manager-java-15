package ru.kolesnikov.tm.command.user;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.util.TerminalUtil;

public class UserUpdateFirstNameCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "user-update-first-name";
    }

    @Override
    public String description() {
        return "Update user first name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE USER FIRST NAME]");
        System.out.println("ENTER NEW USER FIRST NAME: ");
        final String newFirstName = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updateUserFirstName(userId, newFirstName);
        System.out.println("[OK]");
    }

}