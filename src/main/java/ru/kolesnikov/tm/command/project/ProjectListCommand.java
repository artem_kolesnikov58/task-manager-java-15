package ru.kolesnikov.tm.command.project;

import ru.kolesnikov.tm.entity.Project;

import java.util.List;

public class ProjectListCommand extends ProjectAbstractShowCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECTS]");
        final String userId = serviceLocator.getAuthService().getUserId();
        final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        int index = 1;
        for (Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

}