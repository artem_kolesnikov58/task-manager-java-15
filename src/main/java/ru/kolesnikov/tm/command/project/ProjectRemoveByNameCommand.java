package ru.kolesnikov.tm.command.project;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.entity.Project;
import ru.kolesnikov.tm.util.TerminalUtil;

public class ProjectRemoveByNameCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "project-remove-by-name";
    }

    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().removeOneByName(userId, name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}