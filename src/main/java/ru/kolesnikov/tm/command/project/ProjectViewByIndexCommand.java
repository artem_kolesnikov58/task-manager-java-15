package ru.kolesnikov.tm.command.project;

import ru.kolesnikov.tm.entity.Project;
import ru.kolesnikov.tm.util.TerminalUtil;

public class ProjectViewByIndexCommand extends ProjectAbstractShowCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "project-view-by-index";
    }

    @Override
    public String description() {
        return "View project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final String userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

}