package ru.kolesnikov.tm.command.project;

import ru.kolesnikov.tm.entity.Project;
import ru.kolesnikov.tm.util.TerminalUtil;

public class ProjectViewByIdCommand extends ProjectAbstractShowCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "project-view-by-id";
    }

    @Override
    public String description() {
        return "View project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().findOneById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

}