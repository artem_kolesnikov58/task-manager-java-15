package ru.kolesnikov.tm.command.project;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.entity.Project;

public abstract class ProjectAbstractShowCommand extends AbstractCommand {

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

}