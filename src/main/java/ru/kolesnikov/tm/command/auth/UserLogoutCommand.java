package ru.kolesnikov.tm.command.auth;

import ru.kolesnikov.tm.command.AbstractCommand;

public class UserLogoutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "logout";
    }

    @Override
    public String description() {
        return "Logout user.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
    }

}