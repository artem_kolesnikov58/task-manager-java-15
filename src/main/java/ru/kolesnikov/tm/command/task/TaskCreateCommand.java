package ru.kolesnikov.tm.command.task;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getTaskService().create(userId, name, description);
        System.out.println("[OK]");
    }

}