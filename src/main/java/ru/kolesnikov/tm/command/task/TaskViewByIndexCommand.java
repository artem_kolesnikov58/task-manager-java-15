package ru.kolesnikov.tm.command.task;

import ru.kolesnikov.tm.entity.Task;
import ru.kolesnikov.tm.util.TerminalUtil;

public class TaskViewByIndexCommand extends TaskAbstractShowCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "task-view-by-index";
    }

    @Override
    public String description() {
        return "View task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final String userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().findOneByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

}