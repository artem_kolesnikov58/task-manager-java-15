package ru.kolesnikov.tm.command.task;

import ru.kolesnikov.tm.entity.Task;

import java.util.List;

public class TaskListCommand extends TaskAbstractShowCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        final String userId = serviceLocator.getAuthService().getUserId();
        final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        int index = 1;
        for (Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

}