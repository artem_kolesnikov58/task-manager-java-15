package ru.kolesnikov.tm.command.task;

import ru.kolesnikov.tm.entity.Task;
import ru.kolesnikov.tm.util.TerminalUtil;

public class TaskViewByNameCommand extends TaskAbstractShowCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "task-view-by-name";
    }

    @Override
    public String description() {
        return "View task by name.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().findOneByName(userId, name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

}