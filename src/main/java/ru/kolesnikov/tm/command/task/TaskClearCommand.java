package ru.kolesnikov.tm.command.task;

import ru.kolesnikov.tm.command.AbstractCommand;

public class TaskClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getTaskService().clear(userId);
        System.out.println("[OK]");
    }

}