package ru.kolesnikov.tm.repository;

import ru.kolesnikov.tm.api.repository.ICommandRepository;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.command.auth.UserLoginCommand;
import ru.kolesnikov.tm.command.auth.UserLogoutCommand;
import ru.kolesnikov.tm.command.auth.UserRegistrationCommand;
import ru.kolesnikov.tm.command.project.*;
import ru.kolesnikov.tm.command.system.*;
import ru.kolesnikov.tm.command.task.*;
import ru.kolesnikov.tm.command.user.*;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        commandList.add(new ProgramHelpCommand());
        commandList.add(new AboutCommand());
        commandList.add(new ArgumentCommand());
        commandList.add(new CommandCommand());
        commandList.add(new ProgramExitCommand());
        commandList.add(new ProgramVersionCommand());
        commandList.add(new SystemInfoCommand());

        commandList.add(new UserLoginCommand());
        commandList.add(new UserLogoutCommand());
        commandList.add(new UserRegistrationCommand());

        commandList.add(new ProjectClearCommand());
        commandList.add(new ProjectCreateCommand());
        commandList.add(new ProjectListCommand());
        commandList.add(new ProjectRemoveByIdCommand());
        commandList.add(new ProjectRemoveByIndexCommand());
        commandList.add(new ProjectRemoveByNameCommand());
        commandList.add(new ProjectUpdateByIdCommand());
        commandList.add(new ProjectUpdateByIndexCommand());
        commandList.add(new ProjectViewByIdCommand());
        commandList.add(new ProjectViewByIndexCommand());
        commandList.add(new ProjectViewByNameCommand());

        commandList.add(new TaskClearCommand());
        commandList.add(new TaskCreateCommand());
        commandList.add(new TaskListCommand());
        commandList.add(new TaskRemoveByIdCommand());
        commandList.add(new TaskRemoveByIndexCommand());
        commandList.add(new TaskRemoveByNameCommand());
        commandList.add(new TaskUpdateByIdCommand());
        commandList.add(new TaskUpdateByIndexCommand());
        commandList.add(new TaskViewByIdCommand());
        commandList.add(new TaskViewByIndexCommand());
        commandList.add(new TaskViewByNameCommand());

        commandList.add(new UserUpdateEmailCommand());
        commandList.add(new UserUpdateFirstNameCommand());
        commandList.add(new UserUpdateLastNameCommand());
        commandList.add(new UserUpdateMiddleNameCommand());
        commandList.add(new UserUpdatePasswordCommand());
        commandList.add(new UserViewProfileCommand());
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}